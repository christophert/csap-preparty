/*
 * Original work Copyright (c) 2013 TranIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/pr sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Project: 	PreParty
 * Last Edited: 	7/11/2013
 */

/*
 * Christopher Tran / CSAP
 * User: christopher
 * Date: 10/27/13
 * Time: 10:14 PM
 * Host: tranit-rmworkstation.ad.it.chtr.us
 *
 * Private Data:
 *      Static:
 *          COMMANDS:
 *              All valid command symbols
 *      filename: contains filename entered by user.
 *      guestArray: array list of guests
 *      sentinelCheck: boolean value of sentinel
 *
 * Methods: (* indicates private method)
 *      theBass(): the welcome message
 *      theParty(): the exit message
 *      *inputMethod(): Generalized input method that simplifies input throughout the program.
 *      setFileName(): Prompts the user for the filename, checks if valid file and sets the variable filename.
 *      readFile(): Read file and insert guest data from file into guestArray.
 *      *checkIfExists(): Checks if the file exists and if the program is able to read it.
 *      getCommand(): Prompts the user for the desired command, then returns it as an integer.
 *      callCommand(): Receives integer from getCommand and runs the related
 *          method, then recursively calls getCommand().
 *      countGuests(): Counts the number of guests going, maybe, and not going.
 *      *searchByName(): Binary search that searches for a specific guest in the array.
 *      sortArray(): sorts the array.
 *      returnArray(): returns arraylist.
 *      getSentinelCheck(): returns the sentinelCheck variables.
 *      main(): Breaks everything known to man.
 */

import java.io.*;
import java.util.*;

public class Preparty {
    private String filename;
    private static String COMMANDS = "GLNQ";
    private ArrayList<Guest> guestArray = new ArrayList();
    private boolean sentinelCheck = true;

    public Preparty() {
    }

    public void theBass() {
        //Welcome message
        System.out.println("Welcome to the Partay Management System. Here are your list of commands: ");
    }

    public void theParty() {
        //Exit Message
        System.out.println("Everything is ready. TIME TO PARTAY!!");
    }
    private String inputMethod(String prompt) throws IOException {
        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(reader);

        System.out.print(prompt + ": ");
        return input.readLine();
    }

    public void setFileName() throws IOException {
        do {
            filename = inputMethod("Enter the filename");

            if (filename.equals("") || filename == null || !checkIfExists()) {
                System.out.println("File not found or insufficient permissions. Try again.");
            }
        }
        while (filename.equals("") || filename == null || !checkIfExists());
    }

    public void readFile() throws IOException {
        String inputString;

        FileReader reader = new FileReader(filename);
        BufferedReader inFile = new BufferedReader(reader);

        inputString = inFile.readLine();
        while(inputString != null) {
            StringTokenizer st = new StringTokenizer(inputString);
            while(st.hasMoreTokens()) {
                guestArray.add(new Guest(st.nextToken(), st.nextToken(), st.nextToken(), st.nextToken()));
            }
            inputString = inFile.readLine();
        }
    }

    private boolean checkIfExists() throws IOException {
        File check = new File(filename);
        return check.isFile();
    }

    public int getCommand() throws IOException {
        char tempChar;
        String tempInput;
        do {
            System.out.println("G: Guest Information\nL: List Guests\nN: Number of Guests\nQ: Quit");
            tempInput = inputMethod("What would you like to do?").toUpperCase();
            if (tempInput.equals("") || !COMMANDS.contains(tempInput)) {
                System.out.println("Incorrect Command. Please try again.");
            }
        } while (tempInput.equals("") || !COMMANDS.contains(tempInput));

        tempChar = tempInput.charAt(0);
        switch (COMMANDS.indexOf(tempChar)) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            default:
                return 3;
        }
    }

    public void callCommand(int c) throws IOException {
        switch(c) {
            case 0:
                searchByName();
                callCommand(getCommand());
                break;
            case 1:
                returnArray();
                callCommand(getCommand());
                break;
            case 2:
                countGuests();
                callCommand(getCommand());
                break;
            default:
                sentinelCheck = false;
        }
    }

    public void countGuests() {
        int going = 0,
            maybe = 0,
              not = 0;

        System.out.println("Guest Count: ");
        for(Guest var : guestArray) {
            switch (var.getStatus()) {
                case 1:
                    going++;
                    break;
                case 0:
                    not++;
                    break;
                default:
                    maybe++;
            }
        }

        System.out.println("Confirmed: " + going + "\n" + "Pending: " + maybe + "\n" + "Denied: " + not + "\n");
    }

    private void searchByName() throws IOException {
        String last, first, key;
        do {
            last = inputMethod("Last name");
            if(last.equals("") || !last.matches(".*[a-zA-Z].*"))
                System.out.println("Invalid input. Please try again.");
        }while (last.equals("") || !last.matches(".*[a-zA-Z].*"));
        do {
            first = inputMethod("First Name");
            if(first.equals("") || !first.matches(".*[a-zA-Z].*"))
                System.out.println("Invalid input. Please try again.");
        }while (first.equals("") || !first.matches(".*[a-zA-Z].*"));
        key = last + first;
        int low = 0;
        int high = guestArray.size() - 1;
        int mid = (low+high)/2;
        while(low<=high && !(guestArray.get(mid).getFullName(1)).equals(key)) {
            if(key.compareTo(guestArray.get(mid).getFullName(1)) > 0)
                low = mid+1;
            else
                high = mid - 1;
            mid=(low+high)/2;
        }
        if(low>high) {
            System.out.println(first + " " + last +  " was not found in the database.");
        }
        else {
            System.out.println(guestArray.get(mid).getFullName(-1) + " from " + guestArray.get(mid).getCompany() +
                    " " + guestArray.get(mid).getStatusFull() + " going.");
        }
    }

    public void sortArray() {
        Guest temp;
        int count = guestArray.size();
        if(guestArray.size()>0) {
            boolean swap = true;
            while(swap) {
                swap = false;
                count--;
                for(int x=0;x<count;x++) {
                    if(guestArray.get(x).compareTo(guestArray.get(x + 1)) > 0) {
                        temp = guestArray.get(x);
                        guestArray.set(x, guestArray.get(x+1));
                        guestArray.set(x+1, temp);
                        swap = true;
                    }
                }
            }
        }
    }

    public void returnArray() {
        for(Guest var : guestArray)
            var.retGuest();
    }

    public boolean getSentinelCheck() {
        return sentinelCheck;
    }

    public static void main(String args[]) throws IOException {
        Preparty drop = new Preparty();

        drop.theBass();
        drop.setFileName();
        drop.readFile();
        drop.sortArray();

        do {
            drop.callCommand(drop.getCommand());
        }
        while (drop.getSentinelCheck());
        drop.theParty();
    }
}

/*
Welcome to the Partay Management System. Here are your list of commands:
Enter the filename: GuestList.txt
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: L
Field, April from APL maybe going.
Field, Electromagnetic from APL maybe going.
Field, Sally from APL maybe going.
Field, Zoe from APL maybe going.
Geek, Ima from DGT is not going.
Guest, Sally from DGT is not going.
Guest, Zoe from APL is going.
Keys, Qwerty from IBM is not going.
Kulate, Cal from DGT is not going.
List, Linc from APL is going.
Nerd, Ura from IBM maybe going.
Pascal, Blaise from APL is not going.
Queue, Suzy from IBM is not going.
Windoes, Closda from APL is going.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: G
Last name: Geek
First Name: Ima
Ima Geek from DGT is not going.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: G
Last name: List
First Name: Pointoa
Pointoa List was not found in the database.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: G
Last name: Windoes
First Name: Closda
Closda Windoes from APL is going.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: N
Guest Count:
Confirmed: 3
Pending: 5
Denied: 6

G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: G
Last name: Field
First Name: Electromagnetic
Electromagnetic Field from APL maybe going.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: G
Last name: Apple
First Name: Olivia
Olivia Apple was not found in the database.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: G
Last name: Keys
First Name: Asdfgh
Asdfgh Keys was not found in the database.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: G
Last name: Pappoudoupolivsky
First Name: John
John Pappoudoupolivsky was not found in the database.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: G
Last name: Field
First Name: Sally
Sally Field from APL maybe going.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: G
Last name: Field
First Name: April
April Field from APL maybe going.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: G
Last name: Guest
First Name: Ima
Ima Guest was not found in the database.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: X
Incorrect Command. Please try again.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: Y
Incorrect Command. Please try again.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: Z
Incorrect Command. Please try again.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: L
Field, April from APL maybe going.
Field, Electromagnetic from APL maybe going.
Field, Sally from APL maybe going.
Field, Zoe from APL maybe going.
Geek, Ima from DGT is not going.
Guest, Sally from DGT is not going.
Guest, Zoe from APL is going.
Keys, Qwerty from IBM is not going.
Kulate, Cal from DGT is not going.
List, Linc from APL is going.
Nerd, Ura from IBM maybe going.
Pascal, Blaise from APL is not going.
Queue, Suzy from IBM is not going.
Windoes, Closda from APL is going.
G: Guest Information
L: List Guests
N: Number of Guests
Q: Quit
What would you like to do?: Q
Everything is ready. TIME TO PARTAY!!

Process finished with exit code 0
*/