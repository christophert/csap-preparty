/*
 * Original work Copyright (c) 2013 TranIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/pr sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Project: 	PreParty
 * Last Edited: 	13/11/2013
 */

import java.util.ArrayList;

/*
 * Christopher Tran / CSAP
 * User: christopher
 * Date: 10/27/13
 * Time: 10:14 PM
 * Host: tranit-rmworkstation.ad.it.chtr.us
 *
 * Private Data:
 *      Static:
 *          None
 *
 *      first: First Name
 *      last: Last Name
 *      Company: The Apocylapse
 *      status: Status of their invitation
 *
 * Methods:
 *      @override
 *          compareTo:
 *              compares the last names and first names of the guest to see who comes first.
 *      getFirst():
 *          get first name
 *      getLast():
 *          get last name
 *      getCompany():
 *          get company
 *      getStatus():
 *          get status
 *      getStatusFull():
 *          returns status in words
 *      getStatusInt():
 *          returns status in numbers
 *      getFullName(int f):
 *          returns the name based on format provided by f in method call.
 *      retGuest():
 *          prints a single guest's information
 *
 */
public class Guest extends Partay implements Comparable<Guest> {
    private String first, last, company;
    private int status;
    private ArrayList<Integer> relations = new ArrayList<Integer>();

    public Guest(String iFirst, String iLast, String iCompany, String iStatus) {
        first = iFirst;
        last = iLast;
        company = iCompany;
        status = getStatusInt(iStatus);
    }

    @Override
    public int compareTo(Guest g) {
        if(g.last.equals(last)) {
            return first.compareToIgnoreCase(g.first);
        }
        return last.compareToIgnoreCase(g.last);
    }
    public String getFirst() {
        return first;
    }

    public String getLast() {
        return last;
    }

    public String getCompany() {
        return company;
    }

    private int getStatusInt(String status) {
        if(status.equals("yes")) {
            return 1;
        }
        else if(status.equals("no")) {
            return 0;
        }
        else
            return -1;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(String istatus) {
        status = getStatusInt(istatus);
    }

    public String getStatusFull() {
        switch(status) {
            case 1:
                return "is";
            case 0:
                return "is not";
            default:
                return "maybe";
        }
    }

    public ArrayList<Integer> getRelations() {
        return relations;
    }

    public void setRelations(int id) {
        relations.add(id);
    }

    public String getFullName(int f) {   // f for format of name
        switch(f) {
            case 1:
                return last + first;
            case 2:
                return first + last;
            default:
                return first + " " + last;
        }
    }

    public void retGuest() {
        System.out.println(last + ", " + first + " from " + company + " " + getStatusFull() + " going.");
    }
}
