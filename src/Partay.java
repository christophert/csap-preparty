/*
 * Original work Copyright (c) 2013 TranIT
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/pr sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Project: 	PreParty
 * Last Edited: 	13/11/2013
 */

/*
 * Christopher Tran / CSAP
 * User: christopher
 * Date: 10/27/13
 * Time: 10:14 PM
 * Host: tranit-rmworkstation.ad.it.chtr.us
 *
 * Private Data:
 *      Static:
 *          COMMANDS:
 *              All valid command symbols
 *      filename: contains filename entered by user.
 *      guestArray: array list of guests
 *      sentinelCheck: boolean value of sentinel
 *
 * Methods: (* indicates private method)
 *      theBass(): the welcome message
 *      theParty(): the exit message
 *      *inputMethod(): Generalized input method that simplifies input throughout the program.
 *      setFileName(): Prompts the user for the filename, checks if valid file and sets the variable filename.
 *      readFile(): Read file and insert guest data from file into guestArray.
 *      *checkIfExists(): Checks if the file exists and if the program is able to read it.
 *      *addGuest(): Receives input from user and adds guest to array.
 *      *changeResponse(): User will enter search name, it will call searchByName(), and allow them
 *          to change the response of the guest if found.
 *      *getColleagues(): User will enter search name, it will call searchByName(with searchByCompany()) as
  *         a parameter to find all users who belong to that company.
 *      getCommand(): Prompts the user for the desired command, then returns it as an integer.
 *      callCommand(): Receives integer from getCommand and runs the related
 *          method, then recursively calls getCommand().
 *      countGuests(): Counts the number of guests going, maybe, and not going.
 *      *searchByName(): Binary search that searches for a specific guest in the array.
 *      *searchByCompany(): Prints all guests that belong to company provided as a parameter.
 *      sortArray(): sorts the array.
 *      returnArray(): returns arraylist.
 *      getSentinelCheck(): returns the sentinelCheck variables.
 *      main(): Breaks everything known to man.
 */

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Partay {
    private String filename;
    private static final String COMMANDS = "GLNARCMKQ";
    private ArrayList<Guest> guestArray = new ArrayList<Guest>();
    private boolean sentinelCheck = true;

    public Partay() {
    }

    public void theBass() {
        //Welcome message
        System.out.println("Welcome to the Partay Management System. Here are your list of commands: ");
    }

    public void theParty() {
        //Exit Message
        System.out.println("Everything is ready. TIME TO PARTAY!!");
    }
    private String inputMethod(String prompt) throws IOException {
        InputStreamReader reader = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(reader);

        System.out.print(prompt + ": ");
        return input.readLine();
    }

    public void setFileName() throws IOException {
        do {
            filename = inputMethod("Enter the filename");

            if (filename.equals("") || filename == null || !checkIfExists()) {
                System.out.println("File not found or insufficient permissions. Try again.");
            }
        }
        while (filename.equals("") || filename == null || !checkIfExists());
    }

    public void readFile() throws IOException {
        String inputString;

        FileReader reader = new FileReader(filename);
        BufferedReader inFile = new BufferedReader(reader);

        inputString = inFile.readLine();
        while(inputString != null) {
            StringTokenizer st = new StringTokenizer(inputString);
            while(st.hasMoreTokens()) {
                guestArray.add(new Guest(st.nextToken(), st.nextToken(), st.nextToken(), st.nextToken()));
            }
            inputString = inFile.readLine();
        }
    }

    private boolean checkIfExists() {
        File check = new File(filename);
        return check.isFile();
    }

    private void addGuest() throws IOException {
        String[] inputArray = new String[2];
        inputArray[0] = inputMethod("Enter First Name");
        inputArray[1] = inputMethod("Enter Last Name");
        String company = inputMethod("Enter Company");
        String status;
        do {
            status = inputMethod("Enter Attendance Status").toLowerCase();
            if(status.equals("") || !status.equals("yes") && !status.equals("no") && !status.equals("?")) {
                System.out.println("Invalid input. Please enter yes, no, or ? for maybe");
            }
        }
        while(status.equals("") || !status.equals("yes") && !status.equals("no") && !status.equals("?"));

        // f=2 means to print only if the user exists.
        if(searchByName(2, inputArray)[0] == 1) {
            guestArray.add(new Guest(inputArray[0], inputArray[1], company, status));
            sortArray();
            System.out.println("Guest Added.");
        }
        else {
            System.out.println("Guest already exists.");
        }
    }

    private void changeResponse() throws IOException {
        String[] inputArray = new String[2];
        inputArray[0] = inputMethod("First Name");
        inputArray[1] = inputMethod("Last Name");

        int[] returnArray = searchByName(0, inputArray);
        if(returnArray[0] == 0) {
            String tempInput;
            do {
                tempInput = inputMethod("Please enter new invitation status").toLowerCase();

                if(tempInput.equals("") || !tempInput.equals("yes") && !tempInput.equals("no") && !tempInput.equals("?")) {
                    System.out.println("Invalid input. Please enter yes, no, or ? for maybe");
                }
            }
            while(tempInput.equals("") || !tempInput.equals("yes") && !tempInput.equals("no") && !tempInput.equals("?"));
            guestArray.get(returnArray[1]).setStatus(tempInput);
            System.out.println("Status changed.");
        }
        else
            System.out.println("Guest does not exist. Nothing has changed.");
    }

    private void getColleagues() throws IOException {
        String[] inputArray = new String[2];
        inputArray[0] = inputMethod("First Name");
        inputArray[1] = inputMethod("Last Name");
        System.out.println();

        int[] returnArray = searchByName(0, inputArray);
        int statusCode = returnArray[0];
        int userId = returnArray[1];
        if(statusCode == 0) {
            System.out.println("Search Results: ");
            searchByCompany(guestArray.get(userId).getCompany());
        }
        else {
            System.out.println(inputArray[0] + " " + inputArray[1] + " has no friends.");
        }
        System.out.println();
    }
    public int getCommand() throws IOException {
        char tempChar;
        String tempInput;
        do {
            System.out.println("Main Menu");
            System.out.println("G: Guest Information\nL: List Guests\nN: Number of Guests\nA: Add Guest\nR: Change Response\nC: List Colleagues\nM: Add Guest Relations\nK: List Guest Relations\nQ: Quit");
            tempInput = inputMethod("What would you like to do?").toUpperCase();
            if (tempInput.equals("") || !COMMANDS.contains(tempInput)) {
                System.out.println("Incorrect Command. Please try again.");
            }
        } while (tempInput.equals("") || !COMMANDS.contains(tempInput));

        tempChar = tempInput.charAt(0);
        switch (COMMANDS.indexOf(tempChar)) {
            case 0:
                return 0;
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 6;
            case 7:
                return 7;
            default:
                return 8;
        }
    }

    public void callCommand(int c) throws IOException {
        switch(c) {
            case 0:
                searchByName(1, new String[0]);
                callCommand(getCommand());
                break;
            case 1:
                returnArray();
                callCommand(getCommand());
                break;
            case 2:
                countGuests();
                callCommand(getCommand());
                break;
            case 3:
                addGuest();
                callCommand(getCommand());
                break;
            case 4:
                changeResponse();
                callCommand(getCommand());
                break;
            case 5:
                getColleagues();
                callCommand(getCommand());
                break;
            case 6:
                rRelations();
                callCommand(getCommand());
                break;
            case 7:
                getGuestRelations();
                break;
            default:
                sentinelCheck = false;
        }
    }

    public void countGuests() {
        int going = 0,
            maybe = 0,
              not = 0;

        System.out.println("Guest Count: ");
        for(Guest var : guestArray) {
            switch (var.getStatus()) {
                case 1:
                    going++;
                    break;
                case 0:
                    maybe++;
                    break;
                default:
                    not++;
            }
        }

        System.out.println("Confirmed: " + going + "\n" + "Pending: " + maybe + "\n" + "Denied: " + not + "\n");
    }

    private void rRelations() throws IOException{
        String[] inputArray = new String[2];
        int[] guestOne, guestTwo;
        do {
            inputArray[0] = inputMethod("Guest 1 First Name");
            inputArray[1] = inputMethod("Guest 1 Last Name");

            guestOne = searchByName(0, inputArray);

            if(guestOne[0] == 1 || inputArray[0].equals("") || inputArray[1].equals("") || !inputArray[0].matches(".*[a-zA-Z].*") || !inputArray[1].matches(".*[a-zA-Z].*")) {
                System.out.println("Invalid input or guest does not exist. Try again");
            }
        }while(guestOne[0] == 1 || inputArray[0].equals("") || inputArray[1].equals("") || !inputArray[0].matches(".*[a-zA-Z].*") || !inputArray[1].matches(".*[a-zA-Z].*"));

        do {
            inputArray[0] = inputMethod("Guest 2 First Name");
            inputArray[1] = inputMethod("Guest 2 Last Name");

            guestTwo = searchByName(0, inputArray);

            if(guestTwo[0] == 1 || inputArray[0].equals("") || inputArray[1].equals("") || !inputArray[0].matches(".*[a-zA-Z].*") || !inputArray[1].matches(".*[a-zA-Z].*")) {
                System.out.println("Invalid input or guest does not exist. Try again");
            }
        }while(guestTwo[0] == 1 || inputArray[0].equals("") || inputArray[1].equals("") || !inputArray[0].matches(".*[a-zA-Z].*") || !inputArray[1].matches(".*[a-zA-Z].*"));


        if(!guestArray.get(guestOne[1]).getRelations().contains(guestTwo[1])) {
            guestArray.get(guestOne[1]).setRelations(guestTwo[1]);
            guestArray.get(guestTwo[1]).setRelations(guestOne[1]);

            System.out.println("Guest Relation added.");
        }
        else {
            System.out.println("Relation already exists");
        }
    }

    private int[] searchByName(int f, String args[]) throws IOException {
        String last, first, key;

        if(args.length == 0) {
            do {
                last = inputMethod("Last name");
                if(last.equals("") || !last.matches(".*[a-zA-Z].*"))
                    System.out.println("Invalid input. Please try again.");
            }while (last.equals("") || !last.matches(".*[a-zA-Z].*"));
            do {
                first = inputMethod("First Name");
                if(first.equals("") || !first.matches(".*[a-zA-Z].*"))
                    System.out.println("Invalid input. Please try again.");
            }while (first.equals("") || !first.matches(".*[a-zA-Z].*"));
            key = last + first;
        }
        else {
            first = args[0];
            last = args[1];
            key = last + first;
        }
        int low = 0;
        int high = guestArray.size() - 1;
        int mid = (low+high)/2;
        while(low<=high && !(guestArray.get(mid).getFullName(1)).equals(key)) {
            if(key.compareTo(guestArray.get(mid).getFullName(1)) > 0)
                low = mid+1;
            else
                high = mid - 1;
            mid=(low+high)/2;
        }
        if(low>high) {
            if(f == 1)
                System.out.println(first + " " + last +  " was not found in the database.");
            int[] retInfo = new int[2];
            retInfo[0] = 1; //status code (1 - exception occured, 0 - exec ok)
            retInfo[1] = -1; // user id (-1 means none)
            return retInfo;
        }
        else {
            if(f == 1 || f == 2)
                System.out.println(guestArray.get(mid).getFullName(-1) + " from " + guestArray.get(mid).getCompany() + " " + guestArray.get(mid).getStatusFull() + " going.");
            int[] retInfo = new int[2];
            retInfo[0] = 0; //status code
            retInfo[1] = mid; //user id
            return retInfo;
        }
    }

    private void searchByCompany(String companyInput) {
        for(Guest var : guestArray) {
            if(var.getCompany().equals(companyInput)) {
                var.retGuest();
            }
        }
    }

    public void setAllRelations() {
        for(int x=0;x<guestArray.size();x++) {
            for(int y=0;y<guestArray.size();y++) {
                if(guestArray.get(x).getCompany().equals(guestArray.get(y).getCompany()) && x != y) {
                    guestArray.get(x).setRelations(y);
                }
            }
        }
    }

    public void getGuestRelations() throws IOException {
        String[] inputArray = new String[2];
        int[] guest;
        do {
            do {
                inputArray[0] = inputMethod("First name");
                if(inputArray[0].equals("") || !inputArray[0].matches(".*[a-zA-Z].*"))
                    System.out.println("Invalid input. Please try again.");
            }while (inputArray[0].equals("") || !inputArray[0].matches(".*[a-zA-Z].*"));
            do {
                inputArray[1] = inputMethod("Last Name");
                if(inputArray[1].equals("") || !inputArray[1].matches(".*[a-zA-Z].*"))
                    System.out.println("Invalid input. Please try again.");
            }while (inputArray[1].equals("") || !inputArray[1].matches(".*[a-zA-Z].*"));

            guest = searchByName(0, inputArray);

            if(guest[1] == 1) {
                System.out.println("Guest does not exist. Try again.");
            }
        }
        while (guest[0] == 1);
        System.out.println("This guest has relations with: ");
        for(int x=0;x<guestArray.get(guest[1]).getRelations().size();x++) {
            guestArray.get(guestArray.get(guest[1]).getRelations().get(x)).retGuest();
        }
    }

    public void sortArray() {
        Guest temp;
        int count = guestArray.size();
        if(guestArray.size()>0) {
            boolean swap = true;
            while(swap) {
                swap = false;
                count--;
                for(int x=0;x<count;x++) {
                    if(guestArray.get(x).compareTo(guestArray.get(x + 1)) > 0) {
                        temp = guestArray.get(x);
                        guestArray.set(x, guestArray.get(x+1));
                        guestArray.set(x+1, temp);
                        swap = true;
                    }
                }
            }
        }
    }

    public void returnArray() {
        for(Guest var : guestArray)
            var.retGuest();
    }

    public boolean getSentinelCheck() {
        return sentinelCheck;
    }

    public static void main(String args[]) throws IOException {
        Partay drop = new Partay();

        drop.theBass();
        drop.setFileName();
        drop.readFile();
        drop.sortArray();
        drop.setAllRelations();

        do {
            drop.callCommand(drop.getCommand());
        }
        while (drop.getSentinelCheck());
        drop.theParty();
    }
}
